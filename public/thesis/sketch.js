//canvas
var c;
var logo;
var ww;
var wh;

//capture and face detections
var capture;
var faceapi;
var detections;
const detection_options = {
  withLandmarks: true,
  withDescriptors: false,
  minConfidence: 0.5,
  withTinyNet: false,
}

//image containters
var temp_img;

//determines what to happen when the face API model is ready
var done = false;

//determines when to display throbber
var wait = false;

//determines when to look for at face in the capture
var startDetecting = false;

//buttons
var addPic;
var recognize;
var linkToDataSet;
var restart;
var orangeInc;

//Titles
var denied;
var accepted;
var startMsg;

function preload() {
  logo = loadImage("data/orange.png");
}


function setup() {
  c = createCanvas(500, 400);
  c.position(windowWidth/2-width/2, windowHeight/2-height/2);
  ww = windowWidth;
  wh = windowHeight;

  throbber();

  capture = createCapture(VIDEO);
  capture.size(width,height);
  capture.hide();

  createTitles();

  createButtons();

  faceapi = ml5.faceApi(detection_options, modelReady)
}

function draw() {
  if(startDetecting) {
    faceapi.detectSingle(temp_img, gotResults);
    noLoop();
  }
  if(done) {
    background('#CFE9FF');
    push();
    translate(capture.width,0);
    scale(-1,1);
    image(capture, 0, 0, width, height);
    pop();
  }
  if(wait) {
    throbber();
    wait = false;
    startDetecting = true;
  }
}

//creating div elements with titles
function createTitles() {
  denied = createDiv('Your face have been denied entry into the dataset. Your biometrics are already covered by existing data.');
  styleDiv(denied);
  denied.hide();

  accepted = createDiv('Your face was not recognized. Please help improve biometric technology today by donating your face.');
  styleDiv(accepted);
  accepted.hide();

  startMsg = createDiv('Help make The Perfect Dataset™. Donate your face to improve facial detection and recognition software.');
  styleDiv(startMsg);
  startMsg.hide();
}

//styling the div elements
function styleDiv(div) {
  div.size(width,0);
  div.center();
  div.style('background-color','none');
  div.style('color','#0A5EA3');
  var place = wh/2 - height/2-110;
  var placePx = place + 'px 5px';
  div.style('padding',placePx);
  div.style('font-family','Arial');
  div.style('font-size','28px');
  div.style('text-align','center');
}

//creating buttons
function createButtons() {
  //button that is used for donating a face to the dataset
  addPic = createButton('Click here to donate your face to the dataset');
  addPic.position(ww/2 - 188, wh/2+height/2+20);
  styleButton(addPic,'6CC259', '89D179');
  addPic.mousePressed(addPicture);
  addPic.hide();

  //button that is used for initiating the facial detection on the capture
  recognize = createButton('Click here and find out if you get recognized');
  recognize.position(ww/2 - 188, wh/2+height/2+20);
  styleButton(recognize,'0A5EA3', '3571A3');
  recognize.mousePressed(checkPic);
  recognize.hide();

  //button that links to the album with all donated faces
  linkToDataSet = createButton('Click here to see all donated faces');
  linkToDataSet.position(ww/2 - 188, wh/2 + 20);
  styleButton(linkToDataSet, '6CC259', '89D179');
  linkToDataSet.mousePressed(seeDataset);
  linkToDataSet.hide();

  //button that restarts the program
  restart = createButton('Click here to start over');
  restart.position(ww/2 - 188, wh/2+height/2+70);
  styleButton(restart,'0A5EA3', '3571A3');
  restart.mousePressed(restartProgram);
  restart.hide();

  //link that links to the "Orange Inc webpage"
  orangeInc = createA('https://raggedyann.gitlab.io/the-perfect-dataset/OrangeInc/', 'Click here to read about the campaign','_self')
  orangeInc.position(ww/2-125, wh/2+height/2+70);
  orangeInc.style('font-family','Arial');
  orangeInc.style('font-size','15px');
  orangeInc.style('color','#0A5EA3')
  orangeInc.hide();

}

//styling buttons
function styleButton(button,color1, color2) {
  button.size(375,40)
  button.style('background-color','#' + color1);
  button.style('border-style','none');
  button.style('border-radius','28px');
  button.style('display','inline-block');
  button.style('cursor','pointer');
  button.style('color','#ffffff');
  button.style('font-family','Arial');
  button.style('font-size','15px');
  button.style('padding','5px 5px');
  button.style('text-shadow','none');
  button
    .mouseOver(
      function() {
        button.style('background-color', '#' + color2); })
    .mouseOut(
      function() {
        button.style('background-color', '#' + color1); });
}

//styling of a simple throbber
function throbber() {
  noStroke();
  fill(207,233,255,190);
  rect(0,0,width,height);
  textAlign(CENTER);
  textSize(30);
  fill('#0A5EA3');
  text("Loading \nPlease wait...", width/2,height/2);
}

//when the model has loaded
function modelReady() {
  done = true;
  startMsg.show();
  recognize.show();
  orangeInc.show();
}

//captures the current image and starts the facial recognition in draw()
function checkPic() {
  recognize.hide();
  orangeInc.hide();
  startMsg.hide();
  if(done) {
    temp_img = get();
    wait = true;
    done = false;
    loop();
  }
}

//when the face detection has been done
function gotResults(err, result) {
  done = false;
  restart.show();
  if (err) {
    detections = null;
  } else {
    detections = result;
  }

  image(temp_img,0,0);

  if (detections) {
    recognized();
  } else {
    notRecognized();
  }
}

//if a face was recognized
function recognized() {
  denied.show();
  drawBox(detections);
  console.log("face found");
}

//draws a box and a cross over the recognized face
function drawBox(detections){
  const alignedRect = detections.alignedRect;
  const {_x, _y, _width, _height} = alignedRect._box;
  noFill();
  stroke('#F55751');
  strokeWeight(4);
  rect(_x, _y, _width, _height);
  line(_x, _y, _x+_width, _y+_height);
  line(_x + _width, _y, _x, _y+_height);
}

//if a face was not recognized
function notRecognized() {
  accepted.show();
  addPic.show();
  noFill();
  strokeWeight(10);
  stroke('#6CC259');
  rect(2,2, width-4,height-4,5);
  console.log("no face found");
}

//If a picture should be added to the dataset
function addPicture() {
  var isConfirmed;
  isConfirmed = confirm('Terms and Condition for donating your face to The Perfect Dataset™ \nBy uploading your photo to the dataset you agree to the data policy of Orange Inc. Orange Inc. will have permission to use your photo for current and future facial detection and recognition software development projects as it appears of from our Creative Commons license. You thereby also renounce being able to withdraw your donation from the dataset from the time of your upload. \n*This is just an exclaimer, that this is a fictive project made as a critical design theory product for my thesis in the spring term of 2020 in Digital design. Any pictures you actually upload to this project will only be used for this thesis and under the terms of dropbox that hosts my dataset, and you can any time contact me at: a.karring@gmail.com to get your photo removed from the dataset. Orange Inc. is a fictive company made up for the illustrative purpose of critical points made in my thesis. Thank you for helping with my project.*');
  if (isConfirmed) {
    addPic.hide();
    logo.resize(150,0);
    noStroke();
    rectMode(CORNER);
    fill(207,233,255,190);
    rect(0,0,width,height);
    textAlign(CENTER);
    rectMode(CENTER);
    textSize(30);
    fill('#0A5EA3');
    text('Orange Inc. thank you for donating your face and helping create The Perfect Dataset™.', width/2, height/2,width,height/2);
    image(logo, width/2-75,10);
    rectMode(CORNER);
    accepted.hide();
    temp_img.save('DonatedFace' + '-' + year() + '.' + nf(month(),2,0) + '.' + nf(day(),2,0) + '-' + nf(hour(),2,0) + '.' + nf(minute(),2,0) + '.' + nf(second(),2,0),'png');
    restart.position(ww/2 - 188, wh/2 + 70);
    linkToDataSet.show();
    window.open("https://www.dropbox.com/request/hc0um3l4fFf7G4oEAHDY");
  } else {
    restartProgram();
  }
}

//opens the shared dropbox folder with all donated faces
function seeDataset() {
  window.open("https://www.dropbox.com/sh/v84ywz243sftj5d/AAAyT48qUX-YJEc4uGBImvxCa?dl=0");
}

//restarts the program
function restartProgram() {
  restart.position(ww/2 - 188, wh/2+height/2+70);
  restart.hide();
  denied.hide();
  startMsg.show();
  accepted.hide();
  linkToDataSet.hide();
  addPic.hide();
  wait = false;
  startDetecting = false;
  done = true;
  linkToDataSet.hide();
  recognize.show();
  orangeInc.show();
  loop();
}
