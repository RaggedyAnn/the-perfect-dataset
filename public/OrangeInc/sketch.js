var img;
var button;

function preload() {
  img = loadImage("data/orange.png");
}

function setup() {
  createCanvas(windowWidth,windowHeight);
  button = createButton('The Perfect Dataset™ Campaign');
  restart();
}

function restart() {
  background(255);
  img.resize(100,0);
  image(img,0,5);
  noStroke();
  fill(255,125,39);
  textSize(40);
  textFont('Impact');
  text("Orange Inc.", 100, 15, width,img.height);
  fill(30);
  rect(0,75,width,200);
  fill(255);
  textSize(35);
  textFont('Helvetica');
  text("The Perfect Dataset™",100,150);
  textSize(25);
  text("Help improve our facial detection software",100,200);
  fill(0);
  textSize(16);
  text("The Perfect Dataset™ campaign has been set into motion since issues with facial detection has reigned through the last decade concerning the ability to detect a diverse variety of faces. This issue concerns higher fail rates on detecting faces of people with darker skin tones, female characteristics, younger looking features, older looking features, Asian characteristics or changes in features because of disabilities, diseases, disorders, trauma or the alike. \n \nTo combat the issue, Orange Inc. has started this campaign, The Perfect Dataset™. We ask for your help to expand the dataset of faces from online search engines, that we currently use for training our facial detection to include more diverse faces. The Perfect Dataset™ makes it possible for you to help us with this task, by donating your face to our dataset. \n \nClick the button below to be taken to the official campaign page that will take you through the donating process. \n \n \nSincerely, \nThe facial detection team at Orange Inc.", 100,300, width/4*3, height/2);
  button.position(100,height/width*200+500);
  styleButton(button,'0A5EA3', '3571A3');
  button.mousePressed(function () { window.open("https://raggedyann.gitlab.io/the-perfect-dataset/thesis/"); } );
  fill(200);
  rect(0,height-60,width,80);
  fill(255);
  text("Contact Orange Inc. \t Privacy \t Terms of use",100,height-25);
}

function styleButton(button,color1, color2) {
  button.size(300,40)
  button.style('background-color','#' + color1);
  button.style('border-style','none');
  button.style('border-radius','3px');
  button.style('display','inline-block');
  button.style('cursor','pointer');
  button.style('color','#ffffff');
  button.style('font-family','Arial');
  button.style('font-size','15px');
  button.style('padding','5px 5px');
  button.style('text-shadow','none');
  button
    .mouseOver(
      function() {
        button.style('background-color', '#' + color2); })
    .mouseOut(
      function() {
        button.style('background-color', '#' + color1); });
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
  restart();
}

function draw() {
  if (mouseX > 90 && mouseX < 430 && mouseY > height-45) {
    cursor(HAND);
  } else {
    cursor(ARROW);
  }
}

function mousePressed() {
  if (mouseX > 90 && mouseX < 430 && mouseY > height-45) {
    alert('This is a critical design theory product made by Ann Karring for my thesis in the spring term of 2020. Any pictures you actually upload to this project will only be used for this thesis and under the terms of dropbox that hosts my dataset. Orange Inc. is a fictive company made up for the illustrative purpose of critical points made in my thesis. Thank you for supporting my project.')
  }
}
